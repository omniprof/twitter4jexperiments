package com.kenfogel.twitter4jexperiments;

import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import twitter4j.PagableResponseList;
import twitter4j.Query;
import twitter4j.QueryResult;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.User;

/**
 * This is a playground class for experimenting with methods in Twitter4J
 *
 * @author Ken Fogel
 */
public class TwitterExperiments {

    // Real programmers use logging, not System.out.println
    private final static Logger LOG = LoggerFactory.getLogger(TwitterExperiments.class);

    /**
     * Where it begins and we are throwing away exceptions
     *
     * @param args the command line arguments
     * @throws twitter4j.TwitterException
     */
    public static void main(String[] args) throws TwitterException {
        TwitterExperiments bestMethods = new TwitterExperiments();
        bestMethods.perform();
        System.exit(0);
    }
    private final Twitter twitter;

    /**
     * Instantiate the Twitter object
     */
    public TwitterExperiments() {
        twitter = TwitterFactory.getSingleton();
    }

    /**
     * Show the followers/friends of the screen name passed to the method
     *
     * @param screenName
     * @throws TwitterException
     */
    public void showMyFriends(String screenName) throws TwitterException {
        PagableResponseList<User> friends = twitter.getFriendsList(screenName, -1, 50);
        friends.forEach((friend) -> {
            System.out.println(friend.getScreenName() + " " + friend.getFriendsCount() + " " + friend.getFollowersCount());
        });
    }

    // 1187835002517622785
    public int getNumberOfComments() throws TwitterException {
        LOG.debug("getNumberOfComments");
        
        // Notice the string in the query. The to:omniprof
        // means that you want all tweets to omniprof
        Query query = new Query("to:omnipencil");
        
        // This is the ID of the tweet you want to find the comments to
        // It is in the status object. For this experiment I have hard coded it.
        // I have also modified TweetDemo02 to display this id
        query.setSinceId(1187835002517622785L);
        
        // This will retrieve all the tweets that are comments
        QueryResult result = twitter.search(query);
        
        // This turns the QueryResult into a list
        List<Status> statuses = result.getTweets();
        
        // Return the size of the list
        return statuses.size();
        
        // Last two lines can be simplified  to
        //return result.getTweets().size();
    }

    /**
     * Call the method you want to experiment with
     *
     * @throws TwitterException
     */
    public void perform() throws TwitterException {
        //showMyFriends("omnipencil");
        LOG.info("Number of comments = " + getNumberOfComments());
    }
}
